#!/bin/sh
echo Creating documentation
echo "using rst2html, which is in part of the (language)Python docutils package"

dest=`pwd`

mkdir -p $dest/html
rst2html ../README.Grand-prix html/index.shtml

mkdir -p $dest/html/doc
rst2html grand-prix-doc html/doc/grand-prix-doc.html
cp boavista-sample.lisp html/doc/boavista-sample.lisp
