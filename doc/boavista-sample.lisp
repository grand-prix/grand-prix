;; Sample code for boavista, a sample test
(in-package :grand-prix)

(defclass boavista (circuit)
  ()
  (:documentation "Make a database with lots of objects, export it to a backup file, import it to another database."))

(defclass boavista-driver ()
  ())

(defmethod run-circuit ((circuit boavista) (driver boavista))
  (make-clean-db driver)
  (add-some-persons driver)
  (let ((file-path (export-db driver)))
     (add-metric 'backup-file-size (measure-file-length file-path)) ; 2007-feb-07 Not implemented, but would be nice. Ad-hoc numbers to look at.
     (import-to-new-db file-path)
     (verify-persons (query-all-persons-name-and-age driver))))

;; The subclasses of boavista-driver need to implement these
(defgeneric make-clean-db (boavista-driver) (:documentation "Make an empty datbase"))
(defgeneric add-person (boavista-driver name age) (:documentation "Add a person persistent class with an age and a name"))
(defgeneric export-db (boavista-driver) (:documentation "Export a database to a file, any file-format is allowed"))
(defgeneric import-to-new-db (boavista-driver) (:documentation "Import a previously exported database backup to a new clean datbase."))
(defgeneric query-all-persons-name-and-age (boavista-driver) (:documentation "Returns a list of conses (name . age) of all persons in the new db"))



;; Helper functions for run-circuit
(defvar *random-person*) ;; Safe to store here since we know each run-circuit runs in sequence. 
;; Could also be stored as a slot in the boavista circuit class, but this is even easier.

(defun add-some-persons (driver)
  (setf *persons* (make-random-names-and-ages 1000))
  (loop for (name . age) in *persons* do (add-person driver name age)))

(defun verify-persons (returned-persons)
  (assert (not (set-difference *persons* returned-persons))))

(defun make-random-names-and-ages (nr-of-persons)) ;; Left as an exercise
