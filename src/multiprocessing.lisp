(in-package :grand-prix)

;;------------------------------------------------------------------------------
;;  Simple multiprocessing using shell and file communication
;;------------------------------------------------------------------------------
;; The basic idea:
;; 
;; Makes files tmp_process1.lisp ...
;; Starts a lisp using the shell, which loads the tmp_process1.file.

;; Communicates with the tmp_process1.lisp process using small files:
;; Write a lisp form in the tmp_process1.command file.
;; The process will see the file, evaluate the form and write the answer
;; to a new file tmp_process1.answer.
;;------------------------------------------------------------------------------


(defparameter *this-file* *load-truename*)

(defun current-process-id ()
  "Returns an identifier for this process."
  *this-file*)

(defun remove-file-if-exists (pathname)
  (when (cl-fad:file-exists-p pathname)
    (delete-file pathname)))

(defvar *terminate-signal* nil)

(defun process-quit ()
  (setf *terminate-signal* t)
  #+sbcl
  (sb-ext::quit))

(defun process-repl ()
  (print "Process repl started")
  (let ((answer (make-pathname :type "answer" :defaults *this-file*))
        (command (make-pathname :type "command" :defaults *this-file*))
        (err (make-pathname :type "error" :defaults *this-file*))
        (alive (make-pathname :type "alive" :defaults *this-file*)))
    (mapcar #'remove-file-if-exists (list answer command err alive))
    (with-open-file (stream alive :direction :output
                            :if-does-not-exist :create)
      (print (get-universal-time) stream))
    (loop until *terminate-signal* do
          (if (probe-file command)
              (handler-case 
                  (let ((result (with-open-file (in-stream command :direction :input)
                                  (eval (read in-stream)))))

                    (with-open-file (out-stream answer :direction :output
                                                :if-exists :supersede
                                                :if-does-not-exist :create)
                      (print result out-stream))
                    (remove-file-if-exists command))
                (serious-condition (c)
                  (with-open-file (stream err :direction :output
                                          :if-exists :supersede
                                          :if-does-not-exist :create)
                    (princ c stream))
                  (remove-file-if-exists command)))
              (sleep 1)))))


(defun process-repl-disconnected ()
  (process-repl))
  
;;------------------------------------------------------------------------------
(defun read-string-from-file (pathname)
  (with-open-file (in pathname :direction :input)
    (let ((contents (make-sequence 'string (file-length in))))
      (read-sequence contents in)
      contents)))

(defun print-lisp-code (stream &key
                        (first-form '(print "loading"))
                        (last-form '(process-repl-disconnected)))
  (format stream "~S~%~%~a~%~S~%"
          first-form
          (read-string-from-file (make-pathname :type "lisp"
                                                :defaults
                                                *this-file*))
          last-form))

(defclass process ()
  ((nr :initarg :nr :accessor process-nr)
   (state :initform nil)
   (shellprocess :initform nil)))
;;TODO make bookkeeping of nr internal to process. Or use pid instead of nr?

(defun make-process (process-nr)
  (make-instance 'process :nr process-nr))

(defmethod process-lispfile ((p process))
  (make-pathname :type "lisp"
                 :name (format nil "tmp_process~a" (process-nr p))
                 :defaults *this-file*))

(defmethod process-alive-file ((p process))
  (make-pathname :type "alive" :defaults (process-lispfile p)))

(defmethod process-command-file ((p process))
  (make-pathname :type "command" :defaults (process-lispfile p)))

(defmethod process-answer-file ((p process))
  (make-pathname :type "answer" :defaults (process-lispfile p)))

(defmethod process-error-file ((p process))
  (make-pathname :type "error" :defaults (process-lispfile p)))

(defmethod process-io-file ((p process))
  (make-pathname :type "io" :defaults (process-lispfile p)))

(defmethod process-ready ((p process))
  (update-state p)
  (eq :ready (slot-value  p 'state)))

(defmethod update-state ((p process))
  (with-slots (state) p
    (when (and (eq :born state)
               (cl-fad:file-exists-p (process-alive-file p)))
      (setf state :ready))
    (when (and (eq state :executing)
               (not (cl-fad:file-exists-p (process-command-file p))))
      (setf state :ready))))

(defparameter +manual-shell+ nil)

(defmethod start-process ((p process))
  (with-slots (shellprocess state) p
    (let (#+sbcl (cmd (format nil "sbcl --disable-debugger --load '~a'"
                              (process-lispfile p)))
          #-sbcl (error "You need to implement a command in start-process, please help."))
      (remove-file-if-exists (process-command-file p))
      (remove-file-if-exists (process-error-file p))
      (remove-file-if-exists (process-io-file p))
      (remove-file-if-exists (process-answer-file p))
      (remove-file-if-exists (process-alive-file p))
      (if +manual-shell+
          (format t "~%Need manual intervention, run this on the shell:~% ~a~%" cmd)
          (setf shellprocess (trivial-shell::create-shell-process cmd nil)))
      (setf state :born))))

;;TODO: Maybe Move to trivial shell
(defmethod kill-process ((p process))
  (when (process-ready p)
    (start-evaluating-form p '(grand-prix::process-quit)))
  #+sbcl  
  (with-slots (shellprocess) p
    (when shellprocess
      (sleep 1)
      (sb-ext:process-kill shellprocess 9)
      (sleep 1))))

(defmethod start-evaluating-form ((p process) form)
  (when (not (process-ready p))
    (error "Process not ready"))
  (with-open-file (out-stream (process-command-file p)
                              :direction :output
                              :if-exists :supersede
                              :if-does-not-exist :create)
    (princ form out-stream))
  (setf (slot-value p 'state) :executing))

(defmethod process-error-p ((p process))
  (cl-fad:file-exists-p (process-error-file p)))

(defmethod last-result ((p process))
  (with-open-file (in-stream (process-answer-file p) :direction :input)
    (read in-stream)))

;;------------------------------------------------------------------------------
