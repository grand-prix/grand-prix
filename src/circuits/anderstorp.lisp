(in-package :grand-prix)

"http://en.wikipedia.org/wiki/Scandinavian_Raceway

The Scandinavian Raceway is a 2.505-mile race circuit in Anderstorp (Gislaved Municipality), Sweden.

The track was built on marshlands in 1968 and became an extremely popular venue in the 1970s, just as Swede Ronnie Peterson was at the height of his career. It had a long straight (called Flight Straight, which was also used as an aircraft runway), as well as several banked corners, making car setup an engineering compromise.

The raceway hosted six Formula One Swedish Grand Prix events in the '70s. When Peterson and Gunnar Nilsson died during the 1978 Formula One season, public support for the event dried up and the Swedish Grand Prix came to an end. The circuit hosted Touring Car races in the 1980s, and has been a popular car club venue since the 1990s."

(defclass anderstorp (circuit)
  ()
  (:documentation "The rucksack create/update/load tests, but for multiple-processes!"))

(defclass anderstorp-driver ()
  ())

;; Interface for tests
(defgeneric mp-basic-setup (anderstorp-driver) (:documentation "Create 10000 persons objects with name and age"))
(defgeneric mp-check-basic-setup (anderstorp-driver)
  (:documentation "Update all persons in database, set the age to 50, then verify that all is ok"))
(defgeneric mp-asdf-loads (anderstorp-driver) (:documentation "A list of asdf systems that needs to be loaded by each process."))
(defgeneric mp-each-process-does-this-form (anderstorp-driver)
  (:documentation "A lisp form that calls a function that creates 10000 new persons. Note: a lisp form, not a function!"))
(defgeneric mp-check-parallel-process-writes-ok (anderstorp-driver nr-processes)
  (:documentation "Now there should be 10000 * nr-processes plus the basic setup persons in the database"))

;;--------------------------------------------------------------------------------
(defmethod clean-up ((circuit anderstorp))
  (kill-processes))

(defmethod run-circuit ((circuit anderstorp) (driver anderstorp-driver))
  (setf *contender* driver)
  (test-parallel-processes))

;;-------------------- The Anderstorp circuit --------------------

(defun start-processes-and-load-myself (nr-processes)
  (bootstrap-magic :nr-processes nr-processes :asdf-systems-of-testcode (mp-asdf-loads *contender*)))

(defun make-several-processes-write-to-same-database (nr-processes)
  (let ((processes (start-processes-and-load-myself nr-processes)))
    (wait-for-processes :processes processes :timeout 240 :wait-message "Waiting for processes to start")
    (print "Starting tests")
    (mapcar #'(lambda (p)
                    (start-evaluating-form p (mp-each-process-does-this-form *contender*)))
                processes)
    (with-summary (:name (format nil "Writing when nr-processes:~a" nr-processes))
      (with-timer (:name "Time")
        (wait-for-processes :processes processes :timeout 600 :wait-message "Waiting for processes to finish")))
    (print "OK.Finished.")
    (check-all-processes-done processes)))

(defun test-write-to-same-database (&key nr-processes)
  (mp-basic-setup *contender*)
  (mp-check-basic-setup *contender*)
  (unwind-protect
       (make-several-processes-write-to-same-database nr-processes)
    (kill-processes))
  
  (mp-check-parallel-process-writes-ok *contender* nr-processes))

(defun test-parallel-processes ()
  (kill-processes)

  #+nil(setf +manual-shell+ t)
  (test-write-to-same-database :nr-processes 1)
  (print "Ok tests work, now trying with several processes")

  #+nil (setf +manual-shell+ t)
  
  (test-write-to-same-database :nr-processes 2)
  (print "OK. Test successful"))
