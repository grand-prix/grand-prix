(in-package :grand-prix)
;; Control multiprocessing

(defvar *glb-processes* nil)

(defun start-processes (nr-processes asdf-systems &key (one-by-one t))
  (loop repeat nr-processes collecting
        (let ((c (make-process (1+ (length *glb-processes*)))))
          (make-test-file (process-lispfile c) asdf-systems)
          (format t "Process ~a got a lisp-file ~a" (process-nr c) (process-lispfile c))
          (start-process c)
          (when one-by-one
            (wait-for-processes :processes (list c)
                                :wait-message (format nil "Starting process ~a." (process-nr c)))
            (write-line "Now started."))
          (push c *glb-processes*)
          c)))

(defun bootstrap-magic (&key nr-processes asdf-systems-of-testcode)
  (start-processes nr-processes asdf-systems-of-testcode))

(defun kill-processes ()
  (loop for p in *glb-processes* do (kill-process p))
  (setf *glb-processes* nil)
  (sleep 2));; Make sure the OS has time to finish them


(defun wait-for-processes (&key (processes *glb-processes*)(timeout 120)(wait-message "Waiting"))
  (let ((time (get-universal-time)))
    (flet ((processes-ready ()
             (when (and timeout (> (- (get-universal-time) time) timeout))
               (write-line "Timeout. Try loading the tmp_process file manually and see if there are any errors.")
               (error "timeout"))
             (sleep 5)
             (loop for p in processes always (process-ready p))))
      (loop until (processes-ready) do
            (write-line wait-message)))))

(defun check-all-processes-done (&optional (processes *glb-processes*))
  (assert (loop for p in processes
                always (cond
                         ((process-error-p p)
                          (format t "~%Problem with Process:~S.~%Returned error:~a~%"
                                  (process-nr p)
                                  (ignore-errors (read-string-from-file (process-error-file p)))))
                         ((not (eq :ok (last-result p)))
                          (format t "~%Problem with Process:~S.Result was not :ok,it was:~S~%"
                                  (process-nr p)
                                  (last-result p)))
                         (t t)))))


(defun make-asdf-load-forms (asdf-systems)
  (flet ((make-load-forms (asdf-systems)
           (loop for asdf in asdf-systems
                 collect `(progn
                           (asdf:operate 'asdf:load-op ,asdf)
                           (sleep 3)))))
    `(progn ,@(make-load-forms asdf-systems))))

(defun make-test-file (pathname asdf-systems)
  (with-open-file (stream pathname :direction :output :if-exists :supersede )
    (print-lisp-code stream
                     :first-form (make-asdf-load-forms asdf-systems))))


