(in-package :grand-prix)

(defmacro lg (msg &rest args)
  `(progn
    (format t ,msg ,@args)
    (terpri)))

(defun lg-separator ()
  (lg "~%--------------------------------------------------------------------------------"))

