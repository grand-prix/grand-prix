;;; -*- lisp -*-
(in-package :cl-user)

(defpackage :grand-prix-system
    (:use :common-lisp :asdf))

(in-package :grand-prix-system)

(defsystem :grand-prix
  :serial t
  :components ((:file "package")
               (:file "gp-classes")
               (:file "logging")
               (:file "metrics")
	       (:file "multiprocessing")
               (:file "mpcontrol")
               (:module :circuits
                        :components ((:file "anderstorp")))
               (:file "grand-prix"))
  :depends-on (:cl-fad
               ;; :cl-muproc
	       :trivial-shell))
