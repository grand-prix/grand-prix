(in-package :grand-prix)

(defvar *metrics* nil)
(defvar *summary* 0)

(defun reset-metrics ()
  (setf *metrics* nil))

(defun show-metrics ()
  (loop for (name number) in *metrics* do
        (lg "~a~50T~a" name number)))

(defun add-metric(name number)
  (push (list name number) *metrics*))

(defmacro with-summary ((&key name) &body body) ;; todo, optional nr-units unit-name gives average
  `(let ((*summary* 0))
    ,@body
    (add-metric ,name *summary*)))

(defmacro with-timer ((&key name) &body body) ;; todo, optional nr-units unit-name gives average
  (let ((time (gensym)))
    `(let ((,time (get-universal-time)))
      ,@body
      (setf ,time (- (get-universal-time) ,time))
      (incf *summary* ,time)
      (add-metric ,name ,time))))

