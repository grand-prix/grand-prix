(in-package :grand-prix)

(defvar *contender* nil "The active contender")

(defclass circuit ()
  ()
  (:documentation "A set of timed test cases that work against the same data. A test, a race, something that separates a good driver from a bad."))

(defgeneric run-circuit (circuit driver))

(defmethod run-circuit ((circuit circuit) (driver t))
  (format t "Combination not implemented ~a ~a" circuit driver))

(defgeneric laps (circuit)
  (:documentation "All method names for this circuit"))

(defmethod clean-up ((circuit circuit))
  nil)

;;(defclass lap ()
;;  ()
;;  (:documentation "a single test (often timed)"))

(defclass driver ()
  ()
  (:documentation "An implementation of a test for a certain database,an implementation of a circuit for a team"))

;;(defclass team ()
;;  (website :initarg :website :initform nil)
;;  (:documentation "A database competitor. A specific database category or engine that requires specific source code"))
;;
;;(defgeneric drivers (team))
;;

;;(defclass race-car ()
;;  ()
;;  (:documentation "A specialized implementation of a team. A variation/backend"))
;;






