(in-package :grand-prix-rucksack)

(defclass rucksack-anderstorp (grand-prix:anderstorp-driver)
  ()
  (:documentation "Runs the testcase Anderstorp for rucksack standard-cache standard-rucksack. Currently (jan 07) is expected to fail."))

(defclass rucksack-driver (rucksack-anderstorp)
  ()
  (:documentation "Contains all test-mixins for rucksack. Add new testcases to the list of superclasses"))

(eval-when (:compile-toplevel :load-toplevel)
  (grand-prix:register-driver-class 'rucksack-driver))

(defmethod mp-basic-setup ((contender rucksack-anderstorp))
  (my-test-create))

(defmethod mp-check-basic-setup ((contender rucksack-anderstorp))
  (check-basic-setup))

(defmethod mp-each-process-does-this-form ((contender rucksack-anderstorp))
  '(each-process-create-some-persons))

(defmethod mp-asdf-loads ((contender rucksack-anderstorp))
  '(:grand-prix-rucksack))

(defmethod mp-check-parallel-process-writes-ok ((contender rucksack-anderstorp) nr-processes)
  (check-parallel-process-writes-ok nr-processes))

;;------------------------------------------------------------------------------

(defparameter *cache-class* 'rucksack::standard-cache)

(defparameter *rucksack-class* 'rucksack::standard-rucksack)

(defparameter *database* #p"/tmp/rucksack-test-suite-mp/")

(defparameter *names* '("David" "Jim" "Peter" "Thomas"
                        "Arthur" "Jans" "Klaus" "James" "Martin"))

(defclass person ()
  ((name :initform (elt *names* (random (length *names*)))
         :accessor name)
   (age :initform (random 100) :accessor age)
   (made-by :initform (current-process-id))
   (updated-by :initform nil :accessor updated-by))
  (:metaclass persistent-class))

(defmethod print-object ((person person) stream)
  (print-unreadable-object (person stream :type t)
    (format stream "called ~S of age ~D"
            (name person)
            (age person))))

(defparameter *nr-persons* 10000)
(defparameter +age+ 50)

(defun make-persons (rucksack nr-objects)
  (with-transaction ()
    (loop for i below nr-objects
          do (let ((person (make-instance 'person)))
               (when (zerop (mod i 1000))
                 (format t "~D " i))
               (add-rucksack-root person rucksack)))) )

(defun my-test-create ()
  "Test creating a rucksack with many persons."
  (with-rucksack (rucksack *database* :class *rucksack-class* :cache-class *cache-class* :if-exists :supersede :if-does-not-exist :create)
    (make-persons rucksack *nr-persons*)))

(defun my-test-update (&key (new-age 27))
  "Test updating all persons by changing their age."
  (with-rucksack (rucksack *database* :class *rucksack-class* :cache-class *cache-class* )
    (with-transaction ()
     (map-rucksack-roots (lambda (person)
                            (setf (age person) new-age))
                          rucksack))))

(defun my-test-load ()
  "Test loading all persons by computing their average age."
  (let ((nr-persons 0)
        (total-age 0)
        (show-first nil))
    (with-rucksack (rucksack *database* :class *rucksack-class* :cache-class *cache-class*)
      (rollback :rucksack rucksack)
      (with-transaction ()
        (map-rucksack-roots (lambda (person)
                              (incf nr-persons)
                              (when (and show-first (> show-first))
                                (format t "Sample person ~a~%F" show-first)
                                (describe person)
                                (decf show-first))
                              (incf total-age (age person)))
                            rucksack)))
    (values (coerce (/ total-age nr-persons) 'float)
            nr-persons
            total-age)))


(defun check-basic-setup ()
  (my-test-update :new-age +age+)
  (multiple-value-bind (average nr-persons)
      (my-test-load)
    (assert (= +age+ average))
    (assert (= nr-persons *nr-persons*))))


(defun each-process-create-some-persons ()
  (with-rucksack (rucksack *database* :class *rucksack-class* :cache-class *cache-class* )
      (make-persons rucksack *nr-persons*))
  :ok)

(defun check-parallel-process-writes-ok (nr-processes)
  (multiple-value-bind (average nr-persons)
      (my-test-load)
    (declare (ignore average))
    (let ((persons-adjusted-for-basic-setup (- nr-persons *nr-persons*)))
      (format t "Checking writes are ok. ~a persons made by ~a processes each making ~a. Totally ~a persons in database"
              persons-adjusted-for-basic-setup
              nr-processes *nr-persons*
              nr-persons)
      (assert (= persons-adjusted-for-basic-setup 
                 (* *nr-persons* nr-processes))))))
