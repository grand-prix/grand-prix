;;; -*- lisp -*-
(in-package :cl-user)

(defpackage :grand-prix-rucksack-system
    (:use :common-lisp :asdf))

(in-package :grand-prix-rucksack-system)

(asdf:defsystem :grand-prix-rucksack
  :serial t
  :components ((:file "package")
	       (:file "rucksack-anderstorp"))
  :depends-on (:rucksack
	       :grand-prix))
