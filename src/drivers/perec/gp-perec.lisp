(in-package :grand-prix-perec)

;;(defun shell-run (progr &key args)
;;  (port:with-open-pipe (x (apply #'port:pipe-input progr args))
;;    (loop for z = (read-line x nil nil)
;;          while z
;;          collect z)))

(defparameter *user-name* "grandprix")
(defparameter *password* "admin123")
(defparameter *database-name* "perectest")

(defparameter *spec* `(:host "localhost" :database ,*database-name* :user-name ,*user-name* :password ,*password*))

"/usr/bin/createuser --pwprompt  --createdb --echo grandprix"
"/usr/bin/createdb --username grandprix perectest"

;;---------------------------------------------------------------------
;; As usual when it comes to database permissions, it's a pain.
;; 
;; Here is what I had to do on UBUNTU:
;;sudo su postgres -c createuser grandprix
;;  then answered n y y

;;sudo su postgres -c createdb perectest

;;sudo su postgres -c psql perectest
;;
;;ALTER USER grandprix WITH PASSWORD 'admin123';
;;\q
;;---------------------------------------------------------------------


(defvar *database*)

(defun connect ()
  (setf *database*
        (make-instance 'cl-rdbms:postgresql-pg
                       :transaction-mixin 'cl-perec::transaction-mixin
                       :connection-specification *spec*)))

(defun verify-connection ()
  (handler-case (cl-rdbms:with-database *database*
                  (cl-rdbms:with-transaction
                    (cl-rdbms:execute "select 1")
                    (print "Connected.")))
    (condition ()
      (write-line "You need to install postgresql, create the user grandprix with password admin123 and the database perectest."))))




