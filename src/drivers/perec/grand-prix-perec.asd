;;; -*- lisp -*-
(in-package :cl-user)

(defpackage :grand-prix-perec-system
    (:use :common-lisp :asdf))

(in-package :grand-prix-perec-system)

(asdf:defsystem :grand-prix-perec
  :serial t
  :components ((:file "package")
               (:file "gp-perec")
	       (:file "perec-anderstorp"))
  :depends-on (:cl-perec
               :grand-prix))
