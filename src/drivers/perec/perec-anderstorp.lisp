(in-package :grand-prix-perec)

(defclass perec-anderstorp (grand-prix:anderstorp-driver)
  ()
  (:documentation "Runs the testcase Anderstorp for perec."))

(defclass perec-driver (perec-anderstorp)
  ()
  (:documentation "Contains all test-mixins for perec. Add new testcases to the list of superclasses"))

(eval-when (:compile-toplevel :load-toplevel)
  (grand-prix:register-driver-class 'perec-anderstorp))

(defmethod mp-basic-setup ((contender perec-anderstorp))
  (my-test-create))

(defmethod mp-check-basic-setup ((contender perec-anderstorp))
  (check-basic-setup))

(defmethod mp-each-process-does-this-form ((contender perec-anderstorp))
  '(grand-prix-perec::each-process-create-some-persons))

(defmethod mp-asdf-loads ((contender perec-anderstorp))
  '(:grand-prix-perec))

(defmethod mp-check-parallel-process-writes-ok ((contender perec-anderstorp) nr-processes)
  (check-parallel-process-writes-ok nr-processes))

;;------------------------------------------------------------------------------

(defparameter *names* '("David" "Jim" "Peter" "Thomas"
                        "Arthur" "Jans" "Klaus" "James" "Martin"))

(eval-when (:compile-toplevel :load-toplevel :execute)

  (defun define-person-class ()
    "In a function because the drop-table in ensure-clean store messes things up, so we have to define it again"
    (cl-perec:defpclass person ()
      ((name :initform (elt *names* (random (length *names*))) :accessor name) 
       (age :initform (random 100) :accessor age)
       (made-by :initform (grand-prix::current-process-id))
       (updated-by :initform nil :accessor updated-by))))

  (define-person-class))

(defmethod print-object ((person person) stream)
  (print-unreadable-object (person stream :type t)
    (format stream "called ~S of age ~D"
            (name person)
            (age person))))

(defparameter *nr-persons* 10000)  
;; Maybe it's because of the with-transaction, could try it it works with auto-commit instead.
(defparameter +age+ 50)

(defun open-database ()
  (connect)
  (verify-connection))

(defun make-persons (nr-objects)
  (cl-perec:with-transaction
    (loop for i below nr-objects
          do (let ((person (make-instance 'person)))
               (when (zerop (mod i 1000))
                 (format t "~D ~a" i (name person)))
               ))))

(defun ensure-clean-store ()
  (write-line "Cleaning db")
  (cl-perec:with-transaction
    (handler-case
        (cl-rdbms:drop-table '_person)
      (postgresql::error-response (e)
        (declare (ignore e))
        (write-line "Database was already clean."))))
  (write-line "ok")
  (define-person-class))

(defun my-test-create ()
  (open-database)
  (cl-perec:with-database *database*
    (ensure-clean-store)
    (make-persons *nr-persons*)))

(defun my-test-update (&key (new-age 27))
  "Test updating all persons by changing their age."
  (open-database)
  (cl-perec:with-database *database*
    (cl-perec:with-transaction 
      (mapcar #'(lambda (person)
                  (setf (age person) new-age))
              (cl-perec:select ((p person))
                (cl-perec::collect p)
                )))))
;; todo, this can probably be improved, move the lambda to somewhere inside the select

(defun my-test-load ()
  "Test loading all persons by computing their average age."
  (let ((nr-persons 0)
        (total-age 0)
        (show-first nil))
    (open-database)
    (cl-perec:with-database *database*
      (cl-perec:with-transaction
        (mapcar #'(lambda (person)
                    (incf nr-persons)
                    (when (and show-first (> show-first))
                      (format t "Sample person ~a~%F" show-first)
                      (describe person)
                      (decf show-first))
                    (incf total-age (age person)))
                (cl-perec:select ((p person))
                  (cl-perec::collect p)))))
    (values (coerce (/ total-age nr-persons) 'float)
            nr-persons
            total-age)))

(defun check-basic-setup ()
  (my-test-update :new-age +age+)
  (multiple-value-bind (average nr-persons)
      (my-test-load)
    (assert (= +age+ average))
    (assert (= nr-persons *nr-persons*))))


(defun each-process-create-some-persons ()
  (open-database)
  (cl-perec:with-database *database*
    (make-persons *nr-persons*))
  :ok)

(defun check-parallel-process-writes-ok (nr-processes)
  (multiple-value-bind (average nr-persons)
      (my-test-load)
    (declare (ignore average))
    (let ((persons-adjusted-for-basic-setup (- nr-persons *nr-persons*)))
      (format t "Checking writes are ok. ~a persons made by ~a processes each making ~a. Totally ~a persons in database"
              persons-adjusted-for-basic-setup
              nr-processes *nr-persons*
              nr-persons)
      (assert (= persons-adjusted-for-basic-setup 
                 (* *nr-persons* nr-processes))))))
