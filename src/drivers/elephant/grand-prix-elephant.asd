;;; -*- lisp -*-
(in-package :cl-user)

(push :elephant-without-optimize *features*) ;; Until everyhting works ok

(defpackage :grand-prix-elephant-system
    (:use :common-lisp :asdf))

(in-package :grand-prix-elephant-system)


(asdf:defsystem :grand-prix-elephant
  :serial t
  :components ((:file "package")
	       (:file "elephant-anderstorp"))
  :depends-on (:elephant
               :cl-fad
               :grand-prix))

(defmethod asdf:perform :after ((o asdf:load-op) (m (eql (find-system :grand-prix-elephant))))
  ;; Sometimes the elephant loading system bails out, this is for safety.
  (asdf:oos 'asdf:load-op :ele-bdb))
