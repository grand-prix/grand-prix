(in-package :grand-prix-elephant-sql)

(defclass elephant-anderstorp (grand-prix:anderstorp-driver)
  ()
  (:documentation "Runs the testcase Anderstorp for elephant. Currently (jan 07) is expected to fail."))

(defclass elephant-sql-anderstorp (elephant-anderstorp)
  ())

(eval-when (:compile-toplevel :load-toplevel)
  (grand-prix:register-driver-class 'elephant-sql-anderstorp))


(defmethod mp-basic-setup ((contender elephant-sql-anderstorp))
  (ensure-clean-store contender)
  (my-test-create))

(defmethod mp-check-basic-setup ((contender elephant-sql-anderstorp))
  (check-basic-setup))

(defmethod mp-each-process-does-this-form ((contender elephant-sql-anderstorp))
  "(grand-prix-elephant-sql::each-process-create-some-persons)")

(defmethod mp-asdf-loads ((contender elephant-sql-anderstorp))
  '(:grand-prix-elephant-sql))

(defmethod mp-check-parallel-process-writes-ok ((contender elephant-sql-anderstorp) nr-processes)
  (check-parallel-process-writes-ok nr-processes))

;;------------------------------------------------------------------------------

;; sudo su postgres -c createdb elesql

(defparameter *clsql-spec* '("localhost" "elesql" "grandprix" "admin123"))

(defparameter *spec* `(:clsql (:postgresql ,@*clsql-spec*)))

(defmethod ensure-clean-store ((contender elephant-sql-anderstorp))
  (write-line "Cleaning old data")
  (elephant:with-open-store (*spec*)
    (elephant:with-transaction ()
      (elephant:drop-instances (elephant:get-instances-by-class 'person))))
  #+nil
  (sb-sprof:with-profiling (:report :graph)
    (elephant:with-open-store (*spec*)
      (elephant:with-transaction ()
        (elephant:drop-instances (elephant:get-instances-by-class 'person)))))  
  (write-line "ok"))
  


;; (db *clsql-spec* :database-type :postgresql :if-exists :new)      
      ;;      (ignore-errors (clsql:execute-command "delete from keyvalue;" :database db))
      
;;      (ignore-errors (clsql:execute-command "delete from version;" :database db))

    
;;    (ignore-errors (clsql:execute-command "drop table version;" :database db))
;;    (ignore-errors (clsql:execute-command "drop table keyvalue;" :database db))
    
;;    (ignore-errors (clsql:execute-command "drop table version;" :database db))
;;    (ignore-errors (clsql:execute-command "drop sequence serial;" :database db))
;;    (ignore-errors (clsql:execute-command "drop sequence persistent_seq;" :database db))

(defparameter *names* '("David" "Jim" "Peter" "Thomas"
                        "Arthur" "Jans" "Klaus" "James" "Martin"))

(defclass person ()
  ((name :initform (elt *names* (random (length *names*)))
         :accessor name
         :index t) 
;; Actually the index t shouldn't be needed, but since elephant sometimes complained that "person is not an index class", I try if this fixes it.
   (age :initform (random 100) :accessor age)
   (made-by :initform (grand-prix::current-process-id))
   (updated-by :initform nil :accessor updated-by))
  (:metaclass elephant:persistent-metaclass))

(defmethod print-object ((person person) stream)
  (print-unreadable-object (person stream :type t)
    (format stream "called ~S of age ~D"
            (name person)
            (age person))))

(defparameter *nr-persons* 10000)  ;; Should be 10000
;; I think the problem it is becuase the number of locks (999) is = max 1000. see db_stat -e
(defparameter +age+ 50)

(defun make-persons (nr-objects &optional (batch-size 500))
  (loop for i from 1 to (/ nr-objects batch-size) do
        (format t "Batch:~a," i)
        (elephant:with-transaction ()
            (loop for j from 1 to batch-size do
                  (let ((person (make-instance 'person)))
                    (when (zerop (mod (+ (* i batch-size) j) 1000))
                      (format t "~D ~a " (+ (* i batch-size) j) (name person))))))))

;;(defun make-persons (nr-objects)
;;  (loop for i below nr-objects
;;        do (let ((person (elephant:with-transaction ()
;;                           (make-instance 'person))))
;;             (when (zerop (mod i 1000))
;;               (format t "~D ~a" i (name person)))
;;             ))) 



(defun my-test-create ()
  (elephant:with-open-store (*spec*)
    (make-persons *nr-persons*)))
  

;;This version of my test-upated may case errors for large numbers of persons
;;when the number of locks grow to large. Instead, the version below with subsets
;;is the way to go.
;;
#+nil
(defun my-test-update (&key (new-age 27))
  "Test updating all persons by changing their age."
  (elephant:with-open-store (*spec*)
    (elephant:with-transaction ()
      (mapcar #'(lambda (person)
                  (setf (age person) new-age))
              (elephant:get-instances-by-class 'person)))))

(defun subsets (size list)
  (let ((subsets (cons nil nil)))
    (loop for elt in list
          for i from 0 do
          (when (= 0 (mod i size))
            (setf (car subsets) (nreverse (car subsets)))
            (push nil subsets))
          (push elt (car subsets)))
    (setf (car subsets) (nreverse (car subsets)))
    (cdr (nreverse subsets))))

(defmacro do-subsets ((subset subset-size list) &body body)
   `(loop for ,subset in (subsets ,subset-size ,list) do
        ,@body))

(defun my-test-update (&key (new-age 27))
  (format t "Test updating all persons by changing their age.~%")
  (elephant:with-open-store (*spec*)
    (do-subsets (subset 500 (elephant:get-instances-by-class  
                             'person))
      (format t "Doing subset~%")
      (elephant:with-transaction ()
        (mapcar #'(lambda (person)
                    (setf (age person) new-age))
                subset)))))


(defun my-test-load ()
  (format t "Test loading all persons by computing their average age.~%")  
  (let ((nr-persons 0)
        (total-age 0)
        (show-first nil))
    (elephant:with-open-store (*spec*)
      (elephant:with-transaction ()
        (mapcar #'(lambda (person)
                    (incf nr-persons)
                    (when (and show-first (> show-first))
                      (format t "Sample person ~a~%F" show-first)
                      (describe person)
                      (decf show-first))
                    (incf total-age (age person)))
                (elephant:get-instances-by-class 'person))))
    (values (coerce (/ total-age nr-persons) 'float)
            nr-persons
            total-age)))

(defun check-basic-setup ()
  (my-test-update :new-age +age+)
  (multiple-value-bind (average nr-persons)
      (my-test-load)
    (format t "Nr persons ~a (should be ~a)" nr-persons *nr-persons*)
    (assert (= +age+ average))
    (assert (= nr-persons *nr-persons*))))


(defun each-process-create-some-persons ()
  (elephant:with-open-store (*spec*)
    (make-persons *nr-persons*))
  :ok)

(defun check-parallel-process-writes-ok (nr-processes)
  (multiple-value-bind (average nr-persons)
      (my-test-load)
    (declare (ignore average))
    (let ((persons-adjusted-for-basic-setup (- nr-persons *nr-persons*)))
      (format t "Checking writes are ok. ~a persons made by ~a processes each making ~a. Totally ~a persons in database"
              persons-adjusted-for-basic-setup
              nr-processes *nr-persons*
              nr-persons)
      (assert (= persons-adjusted-for-basic-setup 
                 (* *nr-persons* nr-processes))))))
