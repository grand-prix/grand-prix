;;; -*- lisp -*-
(in-package :cl-user)

(push :elephant-without-optimize *features*) ;; Until everyhting works ok

(defpackage :grand-prix-elephant-sql-system
    (:use :common-lisp :asdf))

(in-package :grand-prix-elephant-sql-system)


(asdf:defsystem :grand-prix-elephant-sql
  :serial t
  :components ((:file "package")
	       (:file "elephant-sql-anderstorp"))
  :depends-on (:clsql
               :clsql-postgresql
               :elephant
               :ele-clsql
               :grand-prix))
