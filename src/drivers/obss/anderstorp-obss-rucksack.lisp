(in-package :obss-test-obss-rucksack)

(defparameter *cache-class* 'rucksack::obss-cache)

(defparameter *rucksack-class* 'rucksack::standard-rucksack)

(defparameter *database* #p"/tmp/obss-rucksack-mp/")

(defparameter *names* '("David" "Jim" "Peter" "Thomas"
                        "Arthur" "Jans" "Klaus" "James" "Martin"))

(defclass person ()
  ((name :initform (elt *names* (random (length *names*)))
         :accessor name)
   (age :initform (random 100) :accessor age)
   (made-by :initform (current-process-id))
   (updated-by :initform nil :accessor updated-by))
  (:metaclass persistent-class))

(defmethod print-object ((person person) stream)
  (print-unreadable-object (person stream :type t)
    (format stream "called ~S of age ~D"
            (name person)
            (age person))))

(defparameter *nr-persons* 20) ;; Because the initial performance is low
(defparameter +age+ 50)

(defun my-test-create (&key (nr-objects 20))
  "Test creating a rucksack with many persons."
  (with-rucksack (rucksack *database* :class *rucksack-class* :cache-class *cache-class* :if-exists :supersede :if-does-not-exist :create)
    (with-transaction ()
      (loop for i below nr-objects
            do (let ((person (make-instance 'person)))
                 (when (zerop (mod i 1000))
                   (format t "~D " i))
                 (add-rucksack-root person rucksack))))))

(defun my-test-update (&key (new-age 27))
  "Test updating all persons by changing their age."
  (with-rucksack (rucksack *database* :class *rucksack-class* :cache-class *cache-class* )
    (with-transaction ()
     (map-rucksack-roots (lambda (person)
                            (setf (age person) new-age))
                          rucksack))))

(defun my-test-load ()
  "Test loading all persons by computing their average age."
  (let ((nr-persons 0)
        (total-age 0)
        (show-first nil))
    (with-rucksack (rucksack *database* :class *rucksack-class* :cache-class *cache-class*)
      (rollback :rucksack rucksack)
      (with-transaction ()
        (map-rucksack-roots (lambda (person)
                              (incf nr-persons)
                              (when (and show-first (> show-first))
                                (format t "Sample person ~a~%F" show-first)
                                (describe person)
                                (decf show-first))
                              (incf total-age (age person)))
                            rucksack)))
    (values (coerce (/ total-age nr-persons) 'float)
            nr-persons
            total-age)))

(defun process-create-some-objects ()
  "Each process makes some objects."
  (let ((actually-made 0))
    (with-rucksack (rucksack *database* :class *rucksack-class* :cache-class *cache-class* )
      (with-transaction ()
        (loop for i below *nr-persons*
              do (let ((person (make-instance 'person)))
                   (when (zerop (mod i 1000))
                     (format t "~D " i))
                   (add-rucksack-root person rucksack)
                   (incf actually-made)))))
    actually-made))

(defmethod mp-basic-setup ((contender obss-rucksack-contender))
  (basic-setup))

(defmethod mp-check-basic-setup ((contender obss-rucksack-contender))
  (check-basic-setup))

(defmethod mp-each-process-does-this-form ((contender obss-rucksack-contender))
  '(each-process-do-this))

(defmethod mp-asdf-loads ((contender obss-rucksack-contender))
  '(:obss-test-obss-rucksack))

(defmethod mp-check-parallel-process-writes-ok ((contender obss-rucksack-contender) nr-processes)
  (check-parallel-process-writes-ok nr-processes))

(defun basic-setup()
  (my-test-create :nr-objects *nr-persons*))

(defun check-basic-setup ()
  (my-test-update :new-age +age+)
  (multiple-value-bind (average nr-persons)
      (my-test-load)
    (assert (= +age+ average))
    (assert (= nr-persons *nr-persons*))))


(defun each-process-do-this ()
  (list 'done 'made (process-create-some-objects) 'persons))


(defun check-parallel-process-writes-ok (nr-processes)
  (multiple-value-bind (average nr-persons)
      (my-test-load)
    (declare (ignore average))
    (let* ((made-by-basic-setup *nr-persons*)
          (expected-nr (+ made-by-basic-setup (* *nr-persons* nr-processes))))
      (format t "Checking writes are ok. Counted ~a. Expected ~a. ~a processes each making ~a, plus initial setup."
              nr-persons               
              expected-nr
              nr-processes *nr-persons*)
      (assert (= nr-persons
                 expected-nr)))))
