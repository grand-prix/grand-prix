(in-package :grand-prix)

;;(defparameter *rucksack* (make-instance 'rucksack-contender))

(defvar *available-contenders* nil "Driver class-names are registered in this variable when they are loaded with asdf")

(defun register-driver-class (driver)
  (assert (symbolp driver))
  (pushnew driver *available-contenders*))

(defparameter *contenders* nil)

(defparameter *circuits* '(anderstorp))

;;Todo ideas: Bind *standard-io* for tests and dump it to a file
;;Todo ideas: Take timings for test
;;Todo ideas: Optional better place to store the lg of the test.


(defvar *last-circuit* nil)
(defvar *last-driver* nil)

(defparameter *break-on-errors* nil)


(defun tst (&key (circuit-name *last-circuit*) (driver-name *last-driver*) (clean-up t))
  (let ((driver (make-instance driver-name))
        (circuit (make-instance circuit-name)))
    (unwind-protect
         (reset-metrics)
         (run-circuit circuit driver)
      (when clean-up
        (clean-up circuit)))))


(defun testdrive (circuit-name driver-name)
  (let ((success t))
    (lg-separator)
    (lg "Running: ~a with ~a." circuit-name driver-name )
    (setf *last-circuit* circuit-name *last-driver* driver-name)
    (if *break-on-errors*
        (tst)
        (handler-case
            (progn
              (tst))
          (condition (c)
            (setf success nil)
            (lg "Exception when running ~a with ~a: ~a" circuit-name driver-name c))))
    (lg-separator)
    (lg "Finished: ~a with ~a." circuit-name driver-name )    
    (if success
        (lg "Success!!!!!!!!!!!!!!!!!!!!!")
        (lg "Failure"))
    (show-metrics)
    (lg-separator)
    success))


(defun run-grand-prix ()
  (lg-separator)
  (lg " Running the big object-database Grand Prix!")
  (lg-separator)  
    
  (loop for contender in (or *contenders* *available-contenders*) do
        (unless (member contender *available-contenders*)
          (warn "~a has not been loaded with asdf." contender))
        (loop for circuit in *circuits*
              do (testdrive circuit contender)))
  (lg "The grand-prix has ended.")
  (lg-separator))


;;(run-grand-prix)