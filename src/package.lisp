;;; -*- lisp -*-

(defpackage :grand-prix
  (:use :common-lisp)
  (:export :register-driver-class
           :testdrive
           :run-grand-prix

           ;; Multiprocessing
           :current-process-id

           ;; circuits
           :anderstorp
           :anderstorp-driver

           ;; anderstorp
           :mp-basic-setup
           :mp-check-basic-setup
           :mp-asdf-loads
           :mp-each-process-does-this-form
           :mp-check-parallel-process-writes-ok))





